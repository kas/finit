# finit

## Usage

```
Usage: finit [OPTIONS] [FOO=BAR]...
  Options are:
    -dD, --delimiter=D ..... use 'D' instead of '$' for field delimiter
    -e,  --environment ..... use environment variables as keys
    -fY, --config=Y ........ read defaults from Y (default: ~/.finit/defaults)
    -n,  --no-defaults ..... start with a clear slate by not reading defaults
    -y,  --dump-yaml ....... print mapping in YAML format, then exit
    -h,  --help ............ show this help text and exit
    -v,  --version ......... print version information and exit
    -c,  --copyright ....... show copying policy and exit

This filter replaces all occurrences of '${FOO}' with 'BAR',
where FOO and BAR are taken from commandline argument 'FOO=BAR'.

If the Python YAML module is available, finit will attempt to read default
values from the YAML formatted file ~/.finit/defaults.
```
